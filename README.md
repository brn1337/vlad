# VLAD - Vhd Link Ads Dropper

Hide a .dll into a link using alternate data stream (ADS), then place the .lnk file into an NTFS virtual hard disk (VHD).
The link will run the dll leveraging rundll32.
The malicious dll can't be easily uploaded to VT or similar being in the ADS, however this also means that the link must be send inside the .vhdx.


## Installation

Tested on Powershell v5, needs Hyper-V PowerShell module to handle .vhdx

```powershell
# Install only the PowerShell module
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-Management-PowerShell

# Install the entire Hyper-V stack (hypervisor, services, and tools)
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
```

## Examples

Admin privileges are required to run the script. Don't use paths, just filenames, for the parameters.

```
. .\vlad.ps1

Invoke-GenerateDropper -payload "testDLL.dll" -dllFunc "func" -linkName "CLICK_ME.lnk" -vhdName "CONFIDENTIAL.vhdx" -readonly

Invoke-GenerateDropper -payload "testDLL.dll" -dllFunc "func" -linkName "CLICK_ME.lnk" -vhdName "CONFIDENTIAL.vhdx" -readonly -verbose
```

## Usage

Admin privileges are required to run the script.

```powershell
.PARAMETER payload
    The name of the dll in the current directory (it must be present in cwd).

.PARAMETER dllFunc
    The name of the DLL function to be invoked.

.PARAMETER linkName
    The name for the symbolic link to be created (no paths).

.PARAMETER vhdName
    The name for the VHDX to be created (no paths).

.PARAMETER readonly
    Make the VHDX read-only.

.PARAMETER verbose
    If set, enables verbose output for detailed logging.

.EXAMPLE
    Invoke-GenerateDropper -payload "testDLL.dll" -dllFunc "func" -linkName "CLICK_ME.lnk" -vhdName "CONFIDENTIAL.vhdx" -readonly
```

## License

VLAD is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)

```
CC-BY-NC-SA This license requires that reusers give credit to the creator.
It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only.
If others modify or adapt the material, they must license the modified material under identical terms.
- Credit must be given to you, the creator.
- Only noncommercial use of your work is permitted.
- Adaptations must be shared under the same terms.
```